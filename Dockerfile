FROM node:lts-alpine

WORKDIR /api

COPY package.json .

RUN yarn install

EXPOSE 3000
CMD ["yarn", "start:dev"]
