import { ApiProperty } from "@nestjs/swagger";
import {
  IsString,
  ValidateIf
} from 'class-validator';

export class UserDto {
  @ApiProperty()
  @IsString()
  @ValidateIf(o => o.client == undefined || o.client == "")
  readonly email: string;

  @ApiProperty()
  @IsString()
  @ValidateIf(o => o.client == undefined || o.client == "")
  readonly username: string;

  @ApiProperty()
  @IsString()
  @ValidateIf(o => o.client == undefined || o.client == "")
  readonly password: string;

  readonly superuser: boolean;

  readonly teams: object;
}