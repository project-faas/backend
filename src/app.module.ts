import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersController } from './users/users.controller';
import { UsersService } from './users/users.service';
import { UsersModule } from './users/users.module';

@Module({
  imports: [
    MongooseModule.forRoot("mongodb://root:toor@mongo:27017/main?authSource=admin"),
    UsersModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
