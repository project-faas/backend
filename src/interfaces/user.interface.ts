import { Document } from "mongoose";

export interface User {
  readonly _id: string
  readonly email: string
  readonly username: string
  readonly password: string
  readonly superuser: false
  readonly teams: object
}

export type UserDocument = User & Document;