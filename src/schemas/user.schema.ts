import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';

@Schema()
export class User {
  @Prop({type: String, required: true})
  email: string

  @Prop({type: String, required: true})
  username: string

  @Prop({type: String, required: true})
  password: string

  @Prop({type: Boolean, required: false, default: false})
  superuser: false

  @Prop({type: Object, required: false, default: {}})
  teams: object
}

export const UserSchema = SchemaFactory.createForClass(User);