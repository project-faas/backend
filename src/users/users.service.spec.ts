import { MongooseModule } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { UserDto } from 'src/dtos/user.dto';
import { UserSchema } from '../schemas/user.schema';
import { UsersService } from './users.service';

describe('UsersService', () => {
  let service: UsersService;

  // Change timeout for Jest to allow MongoDB to initialize
  jest.setTimeout(30000);

  let mongodb: MongoMemoryServer;
  let mongoUri: string;

  // Dummy data
  const dummyUser: UserDto = {
    email: 'foobar@email.com',
    username: 'foobar',
    password: 'foo',
    superuser: false,
    teams: {}
  }

  beforeAll(async () => {
    // Initialize a in-memory MongoDB server to run the tests
    mongodb = await MongoMemoryServer.create();
  });

  beforeEach(async () => {
    // Get URI for the in-memory MongoDB database
    if (!mongoUri)
      mongoUri = mongodb.getUri();

    const module: TestingModule = await Test.createTestingModule({
      imports: [
        MongooseModule.forRoot(mongoUri, {
          useUnifiedTopology: true,
          serverSelectionTimeoutMS: 1000
        }),
        MongooseModule.forFeature([{ name: 'User', schema: UserSchema }])
      ],
      providers: [UsersService],
    }).compile();

    service = module.get<UsersService>(UsersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('login() should return an empty object because there is no user with that username', async () => {
    let user = await service.login(dummyUser.username, dummyUser.password);
    expect(user).toBe(null);
  });

  it('create() should create a new user in the database', async () => {
    let newUser = await service.create(dummyUser);
    expect(newUser).toMatchObject(dummyUser);
  });

  it('login() should return the user created in the database, excluding the password', async () => {
    let user = await service.login(dummyUser.username, dummyUser.password);
    expect(user).toMatchObject(user);
    expect(user.password).toBe(undefined);
  });

  it('create() should return an empty object because user already exists on database', async () => {
    let newUser = await service.create(dummyUser);
    expect(newUser).toBe(null);
  });

  it('login() should return an empty object because the password is wrong', async () => {
    let user = await service.login(dummyUser.username, dummyUser.password.concat("1"));
    expect(user).toBe(null);
  });

  afterAll(async () => {
    await mongodb.stop();
  });
});
