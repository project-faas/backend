import { Body, Controller, Get, Post, Query, Res } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { UsersService } from './users.service';
import { UserDto } from '../dtos/user.dto';
import { Response } from 'express';
import { User } from '../schemas/user.schema';

@ApiTags('Users')
@ApiBearerAuth()
@Controller('users')
export class UsersController {
  constructor(private readonly userService: UsersService) {}

  @Post('/create')
  async createNewUser(@Res() res: Response, @Body() userDto: UserDto) {
    try {
      const newUser = await this.userService.create(userDto);
      return res.status(201).json(newUser);
    } catch (err) {
      return res.status(400).json(err);
    }
  }

  @Post('/login')
  async loginUser(@Res() res: Response, @Query('username') username: string, @Query('password') password: string) {
    try {
      const user: User = await this.userService.login(username, password);

      if (user === null) {
        return res.status(404).json("User not found. Username or password must be incorrect!")
      } else {
        return res.status(202).json(user);
      }
    } catch (err) {
      return res.status(400).json(err);
    }
  }
}
