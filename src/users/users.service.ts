import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from '../schemas/user.schema';
import { UserDto } from '../dtos/user.dto';
import { UserDocument } from '../interfaces/user.interface';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel('User') private readonly usersModel: Model<UserDocument>
  ) {}

  public async create(userDto: UserDto): Promise<User> {
    const dbUser = await this.usersModel.findOne({ username: userDto.username });

    if (dbUser !== null) {
      return null;
    }

    const newUser = new this.usersModel(userDto);
    await newUser.save();
    return newUser;
  }

  public async login(username: string, password: string): Promise<User> {
    const dbUser = await this.usersModel.findOne({ username: username });

    let user: User = dbUser;
    if (user !== null && user.password === password) {
      user.password = undefined;
      return user;
    } else {
      return null;
    }
  }
}
