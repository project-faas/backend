import { INestApplication } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { UserSchema } from '../schemas/user.schema';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { UserDto } from '../dtos/user.dto';

describe('UsersController', () => {
  let controller: UsersController;
  let service: UsersService;
  let app: INestApplication;

  // Change timeout for Jest to allow MongoDB to initialize
  jest.setTimeout(30000);

  let mongodb: MongoMemoryServer;
  let mongoUri: string;

  // Dummy data
  const dummyUser: UserDto = {
    email: 'foobar@email.com',
    username: 'foobar',
    password: 'foo',
    superuser: false,
    teams: {}
  }

  beforeAll(async () => {
    // Initialize a in-memory MongoDB server to run the tests
    mongodb = await MongoMemoryServer.create();
  });

  beforeEach(async () => {
    // Get URI for the in-memory MongoDB database
    if (!mongoUri)
      mongoUri = mongodb.getUri();

    const module: TestingModule = await Test.createTestingModule({
      imports: [
        MongooseModule.forRoot(mongoUri, {
          useUnifiedTopology: true,
          serverSelectionTimeoutMS: 1000
        }),
        MongooseModule.forFeature([{ name: 'User', schema: UserSchema }])
      ],
      controllers: [UsersController],
      providers: [UsersService],
      exports: [UsersService]
    }).compile();

    controller = module.get<UsersController>(UsersController);
    service = module.get<UsersService>(UsersService);

    app = module.createNestApplication();
    await app.init();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('/users/create should return 400', () => {
    return request(app.getHttpServer())
      .post('/users/create')
      .expect(400);
  });

  it('/users/login should return 404', () => {
    return request(app.getHttpServer())
      .post('/users/login')
      .query({ username: dummyUser.username, password: dummyUser.password })
      .expect(404);
  });

  it('/users/create should create new user', () => {
    return request(app.getHttpServer())
      .post('/users/create')
      .send(dummyUser)
      .expect(201);
  });

  it('/users/login should return 202', () => {
    return request(app.getHttpServer())
      .post('/users/login')
      .query({ username: dummyUser.username, password: dummyUser.password })
      .expect(202);
  });

  it('/users/login should return 400', async () => {
    await mongodb.stop();
    return request(app.getHttpServer())
      .post('/users/login')
      .expect(400);
  });

  afterAll(async () => {
    await app.close();
  });
});
